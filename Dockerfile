ARG FROM
FROM ${FROM}

ARG SWIFTFORMAT_VERSION
RUN cd /tmp && \
	git clone https://github.com/nicklockwood/SwiftFormat && \
	cd SwiftFormat && \
	git fetch --tags && \
	git checkout ${SWIFTFORMAT_VERSION} -b tag-${SWIFTFORMAT_VERSION} && \
	swift build --configuration release && \
	export SWIFTFORMAT_BIN_PATH=`swift build --configuration release --show-bin-path` && \
	mv "$SWIFTFORMAT_BIN_PATH/swiftformat" "/usr/local/bin/swiftformat" && \
	cd / && \
	rm -rf /tmp/* && \
	swiftformat --version
