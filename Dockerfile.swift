FROM ubuntu:20.04

ARG SWIFT_VERSION
RUN cd /tmp && \
	apt update -y && \
	DEBIAN_FRONTEND="noninteractive" TZ="ETC/UTC" apt install -y clang libicu-dev libpython2.7-dev libtinfo5 libncurses5 libpython2.7 libz3-dev wget libcurl4 libxml2 git && \
	wget -O swift-${SWIFT_VERSION}.tar.gz -c "https://swift.org/builds/swift-${SWIFT_VERSION}-release/ubuntu2004/swift-${SWIFT_VERSION}-RELEASE/swift-${SWIFT_VERSION}-RELEASE-ubuntu20.04.tar.gz" && \
	tar xvzf swift-${SWIFT_VERSION}.tar.gz --strip-components=1 -C / && \
	swift --version && \
	rm -rf /tmp/*
